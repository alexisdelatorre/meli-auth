// http://developers.mercadolibre.com/server-side/

// Token :: { token :: String, created :: Date }

const { Future } = require('fluture')
const { post: _post } = require('request')
const express = require('express')
const { prop, identity, ifElse, type, equals } = require('ramda')
const { readFile, writeFile } = require('fs')
const { encaseN2 } = require('fluture')

const post = url =>
    Future((reject, resolve) => {
        _post(url, (err, httpRes, body) => {
            if (err) reject(err)
            else if (httpRes.statusCode !== 200)
                reject({ status: httpRes.statusCode, body })
            else resolve(body)
        })
    })

// ================== AUTH =======================

const port = 8987

const getCode = id =>
    Future((reject, resolve) => {
        const url = [
            'https://auth.mercadolibre.com.mx/authorization',
            '?',
            'response_type=code',
            `&client_id=${id}`,
            `&redirect_uri=http://localhost:${port}`
        ].join('')

        console.log(`To authenticate open this url on your browser:\n${url}`)

        const app = express()
        const server = app.listen(port)
        app.get('/', (request, response) => {
            response.send('Authenticated, you can close this window now')
            server.close()
            resolve(request.query.code)
        })
    })

const getToken = (code, id, secret) => {
    const url = [
        'https://api.mercadolibre.com/oauth/token',
        '?',
        'grant_type=authorization_code',
        `&client_id=${id}`,
        `&client_secret=${secret}`,
        `&code=${code}`,
        `&redirect_uri=http://localhost:${port}`
    ].join('')

    return post(url)
        .map(JSON.parse)
        .map(prop('access_token'))
        .map(token => ({ token, created: Date.now() }))
}

// ================== CACHE =======================

const isError = x => equals(type(x), 'Error')

const auth = (id, secret) =>
    getCode(id).chain(code => getToken(code, id, secret)).chain(cache)

const cache = token =>
    Future((reject, resolve) => {
        writeFile(
            'token.json',
            JSON.stringify({ token: token.token, created: token.created }),
            err => (err ? reject(err) : resolve(token.token))
        )
    })

const fromCache = (token, id, secret) =>
    Date.now() < token.created + 3 * 60 * 60 * 1000 // cache for 3 hours
        ? Future.of(token.token)
        : auth(id, secret)

module.exports = credentials =>
    encaseN2(readFile, 'token.json', 'utf-8')
      .fold(identity, JSON.parse)
      .chain(ifElse(
        isError,
        () => auth(
          credentials.id || process.env.MELI_CLIENT_ID,
          credentials.secret || process.env.MELI_CLIENT_SECRET
        ),
        token => fromCache(
          token,
          credentials.id || process.env.MELI_CLIENT_ID,
          credentials.secret || process.env.MELI_CLIENT_SECRET
        )
      ))
